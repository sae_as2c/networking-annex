RST_FILE = networking_annex.rst
TEMP_FILE = tmp.rst

all: convert convert_odt

convert:
	./process_table.py $(RST_FILE) $(TEMP_FILE)
	rst2html5.py $(TEMP_FILE) > $(basename $(RST_FILE)).html
	rm $(TEMP_FILE)

convert_odt:
	./process_table.py $(RST_FILE) $(TEMP_FILE)
	rst2odt.py --stylesheet=template/styles.odt --add-syntax-highlighting --generate-oowriter-toc $(TEMP_FILE) > $(basename $(RST_FILE)).odt
	rm $(TEMP_FILE)

clean:
	rm -rf *~

distclean: clean
	rm -rf $(TEMP_FILE)


