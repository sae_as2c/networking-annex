#!/usr/bin/python
# coding: utf-8

import os
import sys
import re

class FileNotFoundException(Exception):
    pass

start_table = re.compile(r"\+=START==(=)*\+").match
med_table   = re.compile(r"\+========(=)*\+").match
end_table   = re.compile(r"\+=END====(=)*\+").match
table_line  = "+-----------------------------------------------------------------------------------------------------------------------------------------+\n"

def preprocess_tables(index_file):
    new_lines = []

    open_braces = 0
    close_braces = 0
    inside_table = 0

    with open(index_file) as current_file:
        for linenum,line in enumerate(current_file):
            if start_table(line):
              inside_table = 1
              new_lines.append(table_line)
            elif med_table(line):
              new_lines.append(table_line)
            elif end_table(line):
              inside_table = 0
              new_lines.append(table_line)
            else:
              if inside_table == 0:
                new_lines.append(line)
              else:
                newline = '|'+line.replace('\n','').ljust(137)+'|\n'
                new_lines.append(newline)

    return new_lines

def run_preprocess(index_file, new_file="temp.rst"):
    if not os.path.exists(index_file):
        print("File not found: %s"%index_file)
        return

    try:
        new_lines = preprocess_tables(index_file)
    except Exception as ex:
        print(ex.message)
        return

    with open(new_file, "w") as write_file:
        for line in new_lines:
            write_file.write("%s" % line)

        write_file.flush()

if __name__ == "__main__":
    original_file = sys.argv[1]
    try:
        write_file = sys.argv[2]
    except IndexError as idxError:
        basedir = os.path.dirname(original_file)
        write_file = os.path.join(basedir, "temp.rst")

    print("original file: '%s'"%original_file)
    print("generated file: '%s'"%write_file)

    run_preprocess(original_file, write_file)

