AADL Networking Annex is defined to support the modeling, analysis and automated configuration of avionics data networks like deterministic Ethernet networks defined in ARINC 664 part 7 (also known as AFDX, AFDX is a trademark of Airbus). It provides AADL architectural style guidelines and AADL defined AFDX oriented properties to define a common approach to use AADL standardized components to express AFDX networks.

Currently, only AFDX networks are described. Later guidance for other networking technologies can be added (e.g. SAE AS6802 (TTEthernet)).
