.. role:: aadl(code)
   :language: aadlid

.. role:: aadldecl(code)
   :language: aadlproperty

Networking Annex
================

.. contents:: Table of Contents

Scope
-----

Networking Annex defines modeling patterns to use the AADL for the
specification of deterministic avionics data networks, as defined by the
ARINC 664 part 7 specification. It also introduces a dedicated property
set to capture specific properties of network components and a library
AADL package containing basic AADL types of network components.

ARINC 664 part 7 is also known as Avionics Full Duplex Switched Ethernet
(AFDX). AFDX is a trademark of Airbus and **???is used with
permission???**.

Rationale
---------

This annex has been defined to support the modeling, analysis and
automated configuration of avionics data networks like deterministic
Ethernet networks defined in ARINC 664 part 7. It provides AADL
architectural style guidelines and AADL defined ARINC 664 oriented
properties to define a common approach to use AADL standardized
components to express ARINC 664 networks.

Environment and Assumptions
---------------------------

Data networks provide their users with communication services. The
services are attributed with some quality of service characteristics
like latency, bandwidth, reliability, etc.

Users of data network services are usually represented in AADL as
software components (:aadl:`PROCESSes` or :aadl:`THREADs`) that communicates
with each other via AADL connections. Another possible situation happens
when there is a stack of communication services and data network
services are consumed by an upper level of the stack that is usually
represented in AADL by :aadl:`VIRTUAL BUS` components.

Data Network
------------

The data network as a whole is modeled with the :aadl:`VIRTUAL BUS`
component extending :aadl:`AFDX::Network` type.

:aadl:`Actual_Connection_Binding` property of connections or virtual buses
should contain a reference to the :aadl:`VIRTUAL BUS` representing AFDX Network
to define that they are users of the AFDX Network communication
services. If :aadl:`Actual_Connection_Binding` property contains references
to a subcomponent of AFDX Network, it should not contain a reference the
Network as a whole.

Hardware Components
-------------------

AFDX network typically includes end systems, switches and duplex wires
connecting ports of them. End systems have two ports por redundancy
purposes. Switches can have many ports. Wires can connects ports of two
switches or a port of end system and a port of a switch.

:aadl:`Actual_Connection_Binding` property of :aadl:`AFDX::Network` component
have to enumerate all components that implements the network. There are
other elements of AFDX network possible in addition of the components
discussed above. For example, if AFDX end system driver is implemented
in software, the software component as well as communication channel
between the driver and the end system device.

There are a couple of properties that describes abilities of hardware
components.

:aadl:`AFDX_Properties::Max_Supported_SubVLs` defines a number of AFDX
Sub-VLs the given end system supports. If an end system does not support
Sub-VLs, :aadl:`AFDX_Properties::Max_Supported_SubVLs` is set to :aadl:`1` that
is a default value for the property.

:aadl:`AFDX_Properties::Supported_Port_Speeds` defines a set of speeds that
the given end system or switch supports. The property is a property of
:aadl:`BUS ACCESSes` to :aadl:`AFDX::Wire`. Usually all the ports of a hardware
component supports the same set of speeds, so the property can be
attached to that component and :aadl:`BUS ACCESSes` inherit it from a
containing component.

AFDX Network Configuration
--------------------------

The main element of configuration of AFDX network is a virtual link that
is a conceptual communication object of AFDX with the following
properties:

-  A Virtual Link defines a logical unidirectional connection from one
   source end-system to one or more destination end-systems.
-  Each Virtual Link has a dedicated maximum bandwidth.

.. raw:: html

   <!-- vls --->

.. raw:: html

   <!-- switches??? --->

Predeclared AADL Property Set for AFDX
--------------------------------------

The :aadl:`AFDX_Properties` property set defines the properties described below.

Virtual Link Properties
~~~~~~~~~~~~~~~~~~~~~~~

+=START===========================================================================================================+

:aadldecl:`BAG : TIME applies to (virtual bus);`

BAG stands for Bandwidth Allocation Gap.
  The End-System controls the transmission flow for each Virtual Link in accordance with the BAG (traffic shaping).
  The Switch verifies the BAG as a part of traffic policing.

  :CONSTRAINTS: power of 2 between 1 ms and 128 ms.
  :MANDATORY: Each AFDX virtual link have to have :aadl:`BAG` property in completely configured network.

+=================================================================================================================+

:aadldecl:`Lmax : AADLINTEGER 64 Bytes .. 1518 Bytes units SIZE_UNITS applies to (virtual bus);`

Maximum frame size for the given Virtual Link.

  :MANDATORY: Each AFDX virtual link have to have :aadl:`Lmax` property in completely configured network.
      Otherwise, 1518 Bytes could be considered as a default value.

+=================================================================================================================+

:aadldecl:`Lmin : AADLINTEGER 64 Bytes .. 1518 Bytes units SIZE_UNITS => 64 Bytes applies to (virtual bus);`

Minimum frame size for the given Virtual Link.

  :OPTIONAL: 64 bytes by default.

+=================================================================================================================+

:aadldecl:`SkewMax : TIME applies to (virtual bus);`

The maximum time between the reception of two redundant frames for the given Virtual Link.
The redundant frames are the same frames sent through both network, A and B.

  :MANDATORY: Each redundant AFDX virtual link have to have :aadl:`SkewMax` property in completely configured network.
      Redundant VL means that the same frames are sent through both network, A and B.

+=================================================================================================================+

.. code:: aadlproperty

  SkewDelayA : TIME => 0 ms applies to (virtual bus);
  SkewDelayB : TIME => 0 ms applies to (virtual bus);


Delay the transmission on one of the ports (A or B) by the skew delay.
  :CONSTRAINTS: Only one of the properties should be more than zero.
  :OPTIONAL: There is no skew delay by default.

+=================================================================================================================+

:aadldecl:`VLID : AADLINTEGER 0..65535 applies to (virtual bus);`

16-bit Virtual Link Identifier.
  :CONSTRAINTS: The identifier should be unique across AFDX network.
  :OPTIONAL: There is no much sense to have the identifiers in
      the model in most cases, as long as they can be generated automatically.

+=END=============================================================================================================+

End System Properties
~~~~~~~~~~~~~~~~~~~~~

+=START===========================================================================================================+

:aadldecl:`Max_Supported_SubVLs : AADLINTEGER => 1 applies to (device,system,processor);`

Sub-VL defines order of delivery of messages via virtual link.
Each Sub-VL has a dedicated FIFO queue, that queues are read on a
round robin basis by the output VL FIFO queue. Sub-VL is an
optional feature of AFDX. A VL FIFO queue should be able to manage
at most 4 Sub-VL FIFO queues.

  :OPTIONAL: All messages go via the only sub-VL by default.

+=================================================================================================================+

:aadldecl:`Supported_Port_Speeds : inherit list of Data_Volume applies to (device,system,processor,bus access);`

List of supported speeds.
  :CONSTRAINTS: 
    | 10 MBpersec,100 MBpersec or 1000 MBpersec.
    | The property makes sense for bus accesses of end system and switch only. 
      Other components are allowed for inheritance purposes only.
    | INHERITABLE to be defined in device for all ports at once.

  :OPTIONAL: It is required only to be able to check consistency.

+=================================================================================================================+

:aadldecl:`portSpeed : inherit Data_Volume applies to (device,system,bus access);`

Speed of the physical port.
  :CONSTRAINTS:
    | It should be in compliance with :aadl:`Supported_Port_Speeds` of End System and Switch.
    | It makes sense for bus accesses of end system and switch only.
      Other components are allowed for inheritance purposes only.
    | INHERITABLE to be defined in one place for the whole system.

  :OPTIONAL: It must be defined at least on one side of each AFDX wire.

+=END=============================================================================================================+

Partition Properties
~~~~~~~~~~~~~~~~~~~~

+=START===========================================================================================================+

:aadldecl:`SubVL : AADLINTEGER applies to (port);`

Sub-VL defines order of delivery of messages via virtual link.
Each Sub-VL has a dedicated FIFO queue, that queues are read on a
round robin basis by the output VL FIFO queue. Sub-VL is an
optional feature of AFDX. A VL FIFO queue should be able to manage
at most 4 Sub-VL FIFO queues.

  :CONSTRAINTS: 
    | It should less or equal :aadl:`Max_Supported_SubVLs` of AFDX End System.
    | It should be applied to output ports only.
    | If a port has subVL all other ports bound to the same virtual link have to have subVL as well.

  :OPTIONAL: All messages go via the only sub-VL by default.

+=================================================================================================================+

:aadldecl:`UDP : AADLINTEGER 1 .. 65535 applies to (port);`

UDP Port.
  :CONSTRAINTS: 
    | If it is applied to output port, it is unique across all output ports of the partition.
    | If it is applied to input port, it is unique across all input ports of the partition.

  :OPTIONAL: There is no much sense to have the explicit UDP port values in the model in many cases,
             as long as they can be generated  automatically.

+=================================================================================================================+

:aadldecl:`PartitionID : AADLINTEGER 0 .. 255 applies to (virtual processor,process);`

8-bit Partition ID.
  It is used to build source (or unicast destination) IP address of partition.

  :CONSTRAINTS: It should be unique across all partitions bound to the same AFDX End System.
  :OPTIONAL: There is no much sense to have the identifiers in the model in many cases,
             as long as they can be generated automatically.

+=END=============================================================================================================+

AFDX Switch Properties
~~~~~~~~~~~~~~~~~~~~~~

+=START===========================================================================================================+

.. code:: aadlproperty

    VL_Route_Table : list of record (
        vl : reference (virtual bus);                 -- MANDATORY: Virtual link
        in_port : reference (bus access);             -- OPTIONAL: can be evaluated from VL bindings
        out_ports : list of reference (bus access);   -- OPTIONAL: can be evaluated from VL bindings
        jitter : TIME;                                -- MANDATORY: Maximum allowed Jitter
        priority : enumeration (high, low);           -- MANDATORY: priority
        accountingPolicy : enumeration (byte, frame); -- MANDATORY: policy
        sharedAccountId : aadlstring;                 -- OPTIONAL: specifying if this account is shared or not
    ) applies to (device,system);

AFDX Switch Configuration Table
  :CONSTRAINTS:
    | It is applicable to AFDX switches only.
    | All referenced virtual links have to be bound to the switch.
    | It should contain entry for any virtual link bound to the switch.
  :OPTIONAL: It is required for configuration generation and detailed analysis only. 

+=================================================================================================================+

.. code:: aadlproperty

  highPriorityQSize : aadlinteger 0 .. Max_Queue_Size => 1 applies to (bus access);
  lowPriorityQSize  : aadlinteger 0 .. Max_Queue_Size => 1 applies to (bus access);

Output Buffer Size for High/Low Priority VLs.
  :CONSTRAINTS: The property applicable to output ports of AFDX switches only.
  :OPTIONAL: The default size is 1.


+=================================================================================================================+

:aadldecl:`maxDelay : inherit TIME applies to (bus access, device, system);`

Maximum delay of frame in switch.

  An output port should not transmit frames that are older than :aadl:`maxDelay`.
  The maximum delay parameter of a frame on a given port is defined as the maximum elapsed time between the two following events:

  1. Arrival of the last bit of a frame on the input port of a switch.
  2. Exit of this last bit of the frame from the given output port of the switch.

  :MANDATORY: It is mandatory for each connected output port of AFDX switch.
  :CONSTRAINTS: 
    | It is applicable to output bus access of AFDX switches only.
    | Other components are allowed for inheritance purposes only.
    | INHERITABLE to be defined in one place for the whole switch.

+=END=============================================================================================================+

Predeclared AADL Package for AFDX Components
--------------------------------------------

The :aadl:`AFDX` package contains a library of basic AADL component types
required to represent AFDX network. Any compliant AADL models of AFDX
networks shall use these types or their extensions.

The library also provides default implementations of component types,
but it is up to model designer to decide if it is appropriate to use
these implementations or to define other ones.

.. code:: aadl

    PACKAGE AFDX
    Public
      WITH AFDX_Properties;
      --...
    END AFDX;

AFDX Network
~~~~~~~~~~~~

.. code:: aadl

      VIRTUAL BUS Network
      END Network;

      VIRTUAL BUS IMPLEMENTATION Network.i
      END Network.i;

:aadl:`VIRTUAL BUS AFDX::Network` represents AFDX network as a whole. There is a predefined implementation of AFDX network :aadl:`AFDX::Network.i`. :aadl:`Actual_Connection_Binding` property of the :aadl:`AFDX::Network` contains a list of references to hardware components included into the network.

Hardware Components
~~~~~~~~~~~~~~~~~~~

.. code:: aadl

      ABSTRACT Switch
      END Switch;

Any AFDX switch implementation have to be extension of :aadl:`ABSTRACT AFDX::Switch` abstract AADL type.

.. code:: aadl

      BUS Wire
      END Wire;

:aadl:`BUS AFDX::Wire` represents physical wires connecting AFDX ports. Every :aadl:`AFDX::Wire` have to be connected to two hardware components. If one of them is :aadl:`AFDX::End_System`, another one have to be :aadl:`AFDX::Switch`. Otherwise, both ends have to be :aadl:`AFDX::Switch`.

.. code:: aadl

      ABSTRACT End_System
        FEATURES
          afdxA : REQUIRES BUS ACCESS Wire;
          afdxB : REQUIRES BUS ACCESS Wire;
      END End_System;

Any AFDX End System implementation have to be extension of :aadl:`ABSTRACT AFDX::End_System` abstract AADL type. It contains two bus accesses to :aadl:`AFDX::Wire`: the first one for network A and the second one for network B.

AFDX Network Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: aadl

      VIRTUAL BUS Virtual_Link
      END Virtual_Link;

      VIRTUAL BUS IMPLEMENTATION Virtual_Link.i
      END Virtual_Link.i;

      VIRTUAL BUS IMPLEMENTATION Virtual_Link.dup
      annex EMV2 {**
        use types     AFDXErrorLib;
        use behavior  AFDXErrorLib::Twostate;

        error propagations
          connection      : in  propagation {NoService};
          bindings        : out propagation {NoService};
        end propagations;
        
        component error behavior
          transitions
            fail : Operational -[ 2 ormore( connection{NoService} ) ]-> Failed;
          propagations
            f : Failed -[]-> bindings(NoService);
        end component;
      **};
      END Virtual_Link.dup;

Any AFDX virtual link have to extends type :aadl:`VIRTUAL BUS AFDX::Virtual_Link`. There are two predefined implementations of this type:

* :aadl:`VIRTUAL BUS IMPLEMENTATION AFDX::Virtual_Link.i` represents a virtual link without redundancy.
* :aadl:`VIRTUAL BUS IMPLEMENTATION AFDX::Virtual_Link.dup` represents a redundant virtual link.

All instances of virtual links have to be placed inside of :aadl:`AFDX::Network` implementation.

:aadl:`Actual_Connection_Binding` property of connections or virtual buses should contain a reference to a :aadl:`VIRTUAL BUS AFDX::Network` or to define that they are users of the AFDX Network communication services.

Any connection or virtual bus that communicates over AFDX network shall have :aadl:`Actual_Connection_Binding` property containing the only reference either to a :aadl:`AFDX::Network` or to a :aadl:`AFDX::Virtual_Link`. The former is used if it is not yet defined which virtual link should handle these communications. The latter is used when the particular virtual link is chosen.

A redundant virtual link :aadl:`AFDX::Virtual_Link.dup` have to bound to a couple of nonredundant virtual links. Any nonredundant virtual link have to be bound to a sequence of hardware components, each of them included into :aadl:`Actual_Connection_Binding` property of the enveloping :aadl:`AFDX::Network`.

Examples
--------

This section presents several examples of the AADL models of simple AFDX
networks.

Software Layer Consuming AFDX Communication Services
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First of all let us define a dummy model of software layer with a few
processes connected via sampling and queuing ports. We will use this
software layer as consumer of AFDX communication services.

.. include:: aadl/Software_Layer.aadl
   :code: aadl

.. figure:: pictures/1-software-layer.png
   :alt: Software Layer AADL Graphical Representation
   :width: 100%

   Software Layer AADL Graphical Representation

Unconfigured AFDX Network
~~~~~~~~~~~~~~~~~~~~~~~~~

The example presents a model describing AFDX Network consisting of
hardware components. Consumers of communication services of the network
has :aadl:`Actual_Connection_Binding` property set to reference to the
network.

Such model can be used as an input for synthesis algorithm that
automatically generates AFDX virtual links and their attributes meeting
requirements (e.g., latency on connections, flows, etc.). The
requirements part is omitted here as long as it is out of scope of the
Networking Annex.

.. include:: aadl/AFDX_PreSynthesis.aadl
   :code: aadl

.. figure:: pictures/2-unconfigured.png
   :alt: Unconfigured AFDX Network Graphical Representation
   :width: 100%

   Unconfigured AFDX Network Graphical Representation


Configured Symmetrical AFDX Network
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The example presents a basic AADL model of a configured AFDX network
that ignores redundancy. It works well for symmetrical Network A and B.

.. include:: aadl/AFDX_Model.aadl
   :code: aadl

.. figure:: pictures/3-symmetrical.png
   :alt: Configured Symmetrical AFDX Network Graphical Representation
   :width: 100%

   Configured Symmetrical AFDX Network Graphical Representation


Configured Asymmetrical AFDX Network
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The example presents an AADL model of a configured AFDX network,
including asymmetrical redundancy network A and B.

.. include:: aadl/AFDX_AsymmModel.aadl
   :code: aadl

.. raw:: html

   <!-- 

   1. ES ports at switches (What is a difference? can be connected to ES?)
   2. ???RedundantVL property - really unredundant VL or just incomplete model


   ???    networkSelector : enumeration (A, B) applies to (device, bus access, virtual bus);

   AFDX Wire actually includes two wires (RX and TX). But is it important?
   Only to emulate situations when one direction fails, while the other one is still alive?



   -->



